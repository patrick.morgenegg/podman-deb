#!/bin/sh
CONMON_VERSION="2.1.0"
CNI_PLUGINS_VERSION="1.1.0"
PODMAN_VERSION="4.0.3"
RUNC_VERSION="1.1.0"

docker build -t conmon-build -f ./conmon/Dockerfile .
docker run --env=CONMON_VERSION=$CONMON_VERSION -v "$(pwd)/packages:/packages" -i conmon-build bash < conmon/build.sh

docker build -t cni-build -f ./cni-plugins/Dockerfile .
docker run --env=CNI_PLUGINS_VERSION=$CNI_PLUGINS_VERSION -v "$(pwd)/packages:/packages" -i cni-build bash < cni-plugins/build.sh

docker build -t podman-build -f ./podman/Dockerfile .
docker run --env=PODMAN_VERSION=$PODMAN_VERSION -v "$(pwd)/packages:/packages" -i podman-build bash < podman/build.sh

docker build -t runc-build -f ./runc/Dockerfile .
docker run --env=RUNC_VERSION=$RUNC_VERSION -v "$(pwd)/packages:/packages" -i runc-build bash < runc/build.sh

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file packages/podman_${PODMAN_VERSION}-1.deb \
     "https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/podman_${PODMAN_VERSION}-1.deb"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file packages/cni-plugins_${CNI_PLUGINS_VERSION}-1.deb \
     "https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/cni-plugins_${CNI_PLUGINS_VERSION}-1.deb"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file packages/conmon_${CONMON_VERSION}-1.deb \
     "https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/conmon_${CONMON_VERSION}-1.deb"

curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
     --upload-file packages/runc_${RUNC_VERSION}-1.deb \
     "https://gitlab.com/api/v4/projects/33996076/packages/generic/podman-deb/1.0.0/runc_${RUNC_VERSION}-1.deb"
